import * as ReactDOM from 'react-dom';
import * as React from 'react';
import { MarvelAppContainer } from '../src';

export class MarvelTaskDemo extends React.PureComponent {
  render() {
    return (
      <div className='marvel-task-demo'>
        <MarvelAppContainer />
      </div>
    );
  }
}

const elem = document.getElementById('demo');
ReactDOM.render(<MarvelTaskDemo />, elem);
