import { Simulate } from 'react-dom/test-utils';

export type BaseDriver = {
    $: (selector: string) => BaseDriver;
    $$: (selector: string) => BaseDriver[];
    text: () => string;
    click: () => void;
    setValue: (val: string) => void;
    getValue: () => string;
    getAttribute: (attr: string) => string;
    hasClass: (cls: string) => boolean;
    isChildVisible: (selector: string) => boolean;
};

export const createBaseDriver = (elem: Element): BaseDriver => {
    return {
        $: (selector: string) => {
            const e = elem.querySelector(selector);
            if (e) {
                return createBaseDriver(e);
            } else {
                throw new Error(`Element with selector ${selector} not found`);
            }
        },
        $$: (selector: string) => {
            const nodes = elem.querySelectorAll(selector);
            if (nodes.length > 0) {
                return Array.from(nodes).map((node) => createBaseDriver(node));
            } else {
                throw new Error(`Did not find any elements with selector ${selector}`);
            }
        },
        text: () => elem.textContent || '',
        click: () => Simulate.click(elem),
        setValue: (val: string) => {
            const inputElem = elem as HTMLInputElement;
            inputElem.value = val;
            Simulate.change(inputElem);
        },
        getValue: () => (elem as HTMLInputElement).value,
        getAttribute: (attr: string) => elem.getAttribute(attr) || '',
        hasClass: (c: string) => elem.classList.contains(c),
        isChildVisible: (selector: string) => !!elem.querySelector(selector)
    };
};
