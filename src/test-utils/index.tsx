import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BaseDriver, createBaseDriver } from './base-driver';

export * from './base-driver';

export const renderAndMountComponent = <T extends {}>(Comp: React.ComponentClass<T> | React.FunctionComponent<T>, props: T): BaseDriver => {
    const elem = document.createElement('div');
    document.body.appendChild(elem);
    ReactDOM.render(<Comp {...props} />, elem);
    return createBaseDriver(elem);
};

export const delay = (ms: number) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
};

export const eventually = async (callback: () => any, timeout: number = 1000, lastErr = {}): Promise<any> => {
    if (timeout < 0) {
        throw new Error(`Timeout exceeded in eventually func: ${lastErr}`);
    }

    try {
        await delay(30);
        return callback();
    } catch (e) {
        const now = Date.now();
        await delay(30);
        const delta = Date.now() - now;
        return eventually(callback, timeout - delta, e);
    }
};

export const noop = () => null as any;

