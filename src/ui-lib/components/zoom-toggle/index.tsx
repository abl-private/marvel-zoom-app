import * as React from 'react';
import styled from 'styled-components';
import { colors } from '../../colors';

export type ZoomToggleProps = {
  value: number;
  onChange: (val: number) => void;
  min: number;
  max: number;
}

class ZoomToggle extends React.PureComponent<ZoomToggleProps> {
  ZOOM_STEP = 0.05;

  onZoomOutClick = () => {
    const newVal = Math.max(this.props.min, this.props.value - this.ZOOM_STEP);
    this.props.onChange(newVal);
  }

  onZoomInClick = () => {
    const newVal = Math.min(this.props.max, this.props.value + this.ZOOM_STEP);
    this.props.onChange(newVal);
  }
  
  render () {
    return (
      <ZoomToggleContainer>
        <ZoomButton className='zoom-out' onClick={this.onZoomOutClick} disabled={this.props.value === this.props.min}>-</ZoomButton>
        <ZoomLabel className='zoom-label'>
          {Math.round(this.props.value * 100)}%
        </ZoomLabel>
        <ZoomButton className='zoom-in' onClick={this.onZoomInClick} disabled={this.props.value === this.props.max}>+</ZoomButton>
      </ZoomToggleContainer>
    )
  }
}

const ZoomToggleContainer = styled.div`
  display: flex;
`;

const ZoomButton = styled.button`
  width: 20px;
  height: 20px;
  background: ${colors.grey};
  color: ${colors.white};
  cursor: pointer;
  outline: 0;
  border: 0;
  border-radius: 50%;

  &:disabled {
    opacity: 0.5;
  }
`;

const ZoomLabel = styled.div`
  min-width: 50px;
  margin: 0 5px;
  color: ${colors.grey};
  text-align: center;
`;

export default ZoomToggle;