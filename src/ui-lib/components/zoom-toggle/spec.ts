import { renderAndMountComponent } from '../../../test-utils/index';
import createZoomToggleDriver, { ZoomToggleDriver } from './driver';
import ZoomToggle, { ZoomToggleProps } from '.';

describe('Zoom Toggle Component', () => {
  const setup = (partial: Partial<ZoomToggleProps>): ZoomToggleDriver => {
    const defaultProps: ZoomToggleProps = {
      value: 0.5,
      onChange: () => null,
      min: 0.5,
      max: 1,
    };

    const comp = renderAndMountComponent(ZoomToggle, {...defaultProps, ...partial});
    return createZoomToggleDriver(comp);
  };

  it('Renders the zoom value label', () => {
    const driver = setup({ value: 0.5 });
    expect(driver.getZoomLabel()).toEqual('50%');
  });

  it('Calls cb with 5% increment when zooming in', () => {
    const onChange = jest.fn();
    const driver = setup({ value: 0.6, onChange });

    driver.clickZoomIn();
    expect(onChange).toHaveBeenCalledWith(0.65);
  });

  it('Calls cb with 5% decrement when zooming out', () => {
    const onChange = jest.fn();
    const driver = setup({ value: 1, onChange });

    driver.clickZoomOut();
    expect(onChange).toHaveBeenCalledWith(0.95);
  });

  it('Does not call cb on zoom in when current value is max zoom (100%)', () => {
    const onChange = jest.fn();
    const driver = setup({ value: 1, onChange, max: 1 });

    driver.clickZoomIn();
    expect(onChange).not.toHaveBeenCalled();
  });

  it('Does not call cb on zoom out when current zoom value is min zoom (50%)', () => {
    const onChange = jest.fn();
    const driver = setup({ value: 0.5, onChange, min: 0.5 });

    driver.clickZoomOut();
    expect(onChange).not.toHaveBeenCalled();
  });
})