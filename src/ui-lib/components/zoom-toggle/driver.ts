import { BaseDriver } from '../../../test-utils/base-driver';

export type ZoomToggleDriver = {
  getZoomLabel: () => string;
  clickZoomIn: () => void;
  clickZoomOut: () => void;
};

const createZoomToggleDriver = (base: BaseDriver): ZoomToggleDriver => ({
  getZoomLabel: () => base.$('.zoom-label').text(),
  clickZoomIn: () => base.$('.zoom-in').click(),
  clickZoomOut: () => base.$('.zoom-out').click(),
});

export default createZoomToggleDriver;
