import * as React from 'react';
import styled from 'styled-components';
import { colors } from '../../colors';

export type SelectItem = {
  id: string;
  label: string;
}

export type SelectProps = {
  value: SelectItem['id'] | null,
  options: SelectItem[];
  onChange: (val: SelectItem) => void;
}

const Select: React.FunctionComponent<SelectProps> = props => {
  const onSelectValueChange = (e: any) => {
    const value = e.target.value as string;
    const item = props.options.find((option) => option.id === value);

    if (item) {
      props.onChange(item);
    }
  }

  return (
    <SelectElem value={props.value || ''} onChange={onSelectValueChange}>
      {props.options.map((option) => <option value={option.id} key={option.id}>{option.label}</option>)}
    </SelectElem>
  )
}

const SelectElem = styled.select`
  color: ${colors.grey};
  border: 1px solid ${colors.grey};
  border-radius: 4px;
  padding: 5px;
  outline: 0;
`;

export default Select;