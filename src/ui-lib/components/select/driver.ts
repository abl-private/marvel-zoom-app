import { BaseDriver } from '../../../test-utils/base-driver';
export type SelectDriver = {
  getSelectedId: () => string;
  getSelectedLabel: () => string;
  selectById: (id: string) => void;
}

const createSelectDriver = (base: BaseDriver): SelectDriver => ({
  getSelectedId: () => base.$('select').getValue(),
  getSelectedLabel: () => {
    const id = base.$('select').getValue();
    return base.$(`option[value="${id}"]`).text();
  },
  selectById: (id: string) => base.$('select').setValue(id),
})

export default createSelectDriver;