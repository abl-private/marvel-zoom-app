import { renderAndMountComponent } from '../../../test-utils';
import Select, { SelectProps } from '.';
import createSelectDriver, { SelectDriver } from './driver';

describe('Select component', () => {
  const setup = (partial: Partial<SelectProps>): SelectDriver => {
    const defaultProps: SelectProps = {
      value: '1',
      options: [{ id: '1', label: 'option 1'}],
      onChange: () => null,
    };

    const comp = renderAndMountComponent(Select, {...defaultProps, ...partial});
    return createSelectDriver(comp);
  };

  it('Renders the selected value', () => {
    const options = [
      { id: 'item1', label: 'Item 1' },
      { id: 'item2', label: 'Item 2' },
    ]
    const driver = setup({ value: 'item1', options });
    expect(driver.getSelectedId()).toEqual('item1');
    expect(driver.getSelectedLabel()).toEqual('Item 1');
  });

  it('Calls cb when changing the selected item', () => {
    const onChange = jest.fn();
    const options = [
      { id: 'item1', label: 'Item 1' },
      { id: 'item2', label: 'Item 2' },
    ]
    const driver = setup({ value: 'item1', options, onChange });

    driver.selectById('item2');
    expect(onChange).toHaveBeenCalledWith(options[1]);
  });
})