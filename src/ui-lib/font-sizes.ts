export const fontSizes = {
  small: 12,
  normal: 16,
  large: 24,
};