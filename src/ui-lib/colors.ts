export const colors = {
  white: '#ffffff',
  whiteD10: '#FDFDFD',
  grey: '#AAB2BF',
  greyL10: '#E7E7E7',
  greyL20: '#f5f5f5',
  greyL30: '#ECF0F1',
  black: '#14233C',
  red: '#FF4D56',
  green: '#46E1A2',
  blue: '#1DAAFC',
  blueD10: '#334256',
};
