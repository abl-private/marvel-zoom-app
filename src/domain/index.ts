export type Project = {
  id: number;
  name: string;
  screens: ImageScreen[];
}

// I am assuming that displayName is unique
export type ImageScreen = {
  displayName: string;
  content: ImageScreenContent;
}

export type ImageScreenContent = {
  filename: string;
  url: string;
  height: number;
  width: number;
}