import { Project, ImageScreen, ImageScreenContent } from './index';

export const buildProject = (partial: Partial<Project> = {}): Project => ({
  id: 1,
  name: 'Fake Project!',
  screens: [],
  ...partial
});

export const buildImageScreen = (partial: Partial<ImageScreen> = {}): ImageScreen => ({
  displayName: 'Fake Image Screen!',
  content: buildImageScreenContent(),
  ...partial
});

export const buildImageScreenContent = (partial: Partial<ImageScreenContent> = {}): ImageScreenContent => ({
  filename: 'image.png',
  url: 'https://mydomain.com/image.png',
  height: 100,
  width: 100,
  ...partial,
})