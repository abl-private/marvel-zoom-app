import { MarvelApi } from './index';
import { buildProject } from '../domain/builders';

export const createMarvelMockApi = (): MarvelApi => ({
  getProjectById: () => Promise.resolve(buildProject()),
});
