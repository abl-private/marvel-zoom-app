import { Project } from '../domain';
import axios from 'axios';

export type MarvelApi = {
  getProjectById: (id: number) => Promise<Project>;
};

export const createMarvelGQLApi = (apiUrl: string, authToken: string): MarvelApi => {
  const client = axios.create({
    headers: {
      Authorization: `Bearer ${authToken}`,
      'Content-Type': 'application/json',
    }
  });

  return {
    getProjectById: async (id) => {
      const query = `{
        project(pk: ${id}) {
          pk
          name
          screens {
            edges {
              node {
                displayName
                content {
                  ... on ImageScreen {
                    filename
                    url
                    height
                    width
                  }
                }
              }
            }
          }
        }
      }`;

      try {
        const gqlData = await client.post(apiUrl, { query }).then(r => r.data.data);
        const screenEdges: any[] = gqlData.project.screens.edges || [];
  
        return {
          id: gqlData.project.pk,
          name: gqlData.project.name,
          screens: screenEdges.map((edge) => edge.node),
        }
      } catch (e) {
        throw `Unable to retrieve data from Marvel GraphQL api: ${e}`;
      }
    }
  }
};
