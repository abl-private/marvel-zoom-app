import { MarvelAppDriver, createMarvelAppDriver } from './driver';
import { renderAndMountComponent } from '../test-utils';
import { MarvelApp, MarvelAppProps } from '.';
import { createMarvelMockApi } from '../api/mock';
import { buildProject, buildImageScreen, buildImageScreenContent } from '../domain/builders';

describe('Marvel app', () => {
  const setup = (partial: Partial<MarvelAppProps> = {}): MarvelAppDriver => {
    const props = {
      api: createMarvelMockApi(),
      ...partial
    };

    const base = renderAndMountComponent(MarvelApp as any, props);
    return createMarvelAppDriver(base);
  };

  it('Calls the Marvel API to retrieve the project for the provided id', () => {
    const api = createMarvelMockApi();
    const project = buildProject({ id: 123 });
    api.getProjectById = jest.fn(() => Promise.resolve(project));

    setup({ api, projectId: project.id });
    expect(api.getProjectById).toHaveBeenCalledWith(project.id);
  });

  it('Changes the currently selected image', async () => {
    const api = createMarvelMockApi();
    const screen1 = buildImageScreen({ displayName: 'Screen 1' });
    const screen2 = buildImageScreen({ displayName: 'Screen 2' });

    const screens = [screen1, screen2];
    const project = buildProject({ screens });
    api.getProjectById = jest.fn(() => Promise.resolve(project));

    const driver = await setup({ api, projectId: project.id });

    expect(driver.header().screenSelect().getSelectedId()).toEqual('Screen 1');
    driver.header().screenSelect().selectById('Screen 2');
    expect(driver.header().screenSelect().getSelectedId()).toEqual('Screen 2');
  });

  it('Renders correct relative image dimensions when changing images', async () => {
    // Mock calculation of viewport dimensions
    const dimensions = { width: 300, height: 300, top: 0, bottom: 0, x: 0, y: 0, left: 0, right: 0 };
    Element.prototype.getBoundingClientRect = jest.fn(() => (dimensions));

    const api = createMarvelMockApi();
    const c1 = buildImageScreenContent({ width: 200, height: 200 });
    const c2 = buildImageScreenContent({ width: 400, height: 400 });
    const screen1 = buildImageScreen({ displayName: 's1', content: c1 });
    const screen2 = buildImageScreen({ displayName: 's2', content: c2 });
    const project = buildProject({ screens: [screen1, screen2] });
    api.getProjectById = jest.fn(() => Promise.resolve(project));

    const driver = await setup({ api, projectId: project.id });

    expect(driver.imageScreen().getImageWidth()).toEqual(200);
    expect(driver.header().zoomToggle().getZoomLabel()).toEqual('100%');
    driver.header().zoomToggle().clickZoomOut();
    expect(driver.imageScreen().getImageWidth()).toEqual(200 * 0.95);

    driver.header().screenSelect().selectById('s2');
    expect(driver.imageScreen().getImageWidth()).toEqual(300);
    expect(driver.header().zoomToggle().getZoomLabel()).toEqual('75%');
  });
});
