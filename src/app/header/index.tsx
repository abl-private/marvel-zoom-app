import * as React from 'react';
import styled from 'styled-components';
import { Project } from '../../domain/index';
import Select, { SelectItem } from '../../ui-lib/components/select';
import ZoomToggle from '../../ui-lib/components/zoom-toggle';
import { colors } from '../../ui-lib';

export type HeaderProps = {
  project?: Project;
  selectedScreenId?: string;
  onSelectScreen: (id: string) => void;
  zoom: number;
  onChangeZoom: (val: number) => void;
}

class Header extends React.PureComponent<HeaderProps> {
  MIN_ZOOM_VALUE = 0.5;
  MAX_ZOOM_VALUE = 1;

  getSelectOptions = () => {
    if (!this.props.project) return [];
    return this.props.project.screens.map((screen) => ({ id: screen.displayName, label: screen.displayName }));
  }

  onChangeImageScreen = (item: SelectItem) => this.props.onSelectScreen(item.id);

  render () {
    return (
      <HeaderContainer>
        <Select
          value={this.props.selectedScreenId || ''}
          options={this.getSelectOptions()}
          onChange={this.onChangeImageScreen}
        />
        <ZoomToggle
          value={this.props.zoom}
          onChange={this.props.onChangeZoom}
          min={this.MIN_ZOOM_VALUE}
          max={this.MAX_ZOOM_VALUE}
        />
      </HeaderContainer>
    )
  }
}

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px;
  border: 1px solid ${colors.grey};
`;

export default Header;