import { HeaderProps } from './index';
import { HeaderDriver } from './driver';
import { renderAndMountComponent } from '../../test-utils';
import createHeaderDriver from './driver';
import Header from './index';
import { buildProject, buildImageScreen } from '../../domain/builders';

describe('App Header', () => {
  const setup = (partial: Partial<HeaderProps> = {}): HeaderDriver => {
    const project = buildProject();
    const props: HeaderProps = {
      project,
      selectedScreenId: '',
      onSelectScreen: () => null,
      zoom: 1,
      onChangeZoom: () => null,
      ...partial
    };

    const base = renderAndMountComponent(Header as any, props);
    return createHeaderDriver(base);
  };

  it('Shows the selected project screen and calls cb when changed', () => {
    const s1 = buildImageScreen({ displayName: 's1' });
    const s2 = buildImageScreen({ displayName: 's2' });
    const project = buildProject({ screens: [s1, s2] });
    const onSelectScreen = jest.fn();
    const driver = setup({ project, selectedScreenId: 's1', onSelectScreen });

    expect(driver.screenSelect().getSelectedId()).toEqual('s1');
    driver.screenSelect().selectById('s2');
    expect(onSelectScreen).toHaveBeenCalledWith('s2');
  });

  it('Shows the selected zoom and calls cb when changed', () => {
    const project = buildProject();
    const onChangeZoom = jest.fn();
    const driver = setup({ project, zoom: 0.5, onChangeZoom });

    expect(driver.zoomToggle().getZoomLabel()).toEqual('50%');
    driver.zoomToggle().clickZoomIn();
    expect(onChangeZoom).toHaveBeenLastCalledWith(0.55);
  });
})