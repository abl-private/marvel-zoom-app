import createSelectDriver, { SelectDriver } from '../../ui-lib/components/select/driver';
import { ZoomToggleDriver } from '../../ui-lib/components/zoom-toggle/driver';
import { BaseDriver } from '../../test-utils/base-driver';
import createZoomToggleDriver from '../../ui-lib/components/zoom-toggle/driver';

export type HeaderDriver = {
  screenSelect: () => SelectDriver;
  zoomToggle: () => ZoomToggleDriver;
}

const createHeaderDriver = (base: BaseDriver): HeaderDriver => ({
  screenSelect: () => createSelectDriver(base),
  zoomToggle: () => createZoomToggleDriver(base),
});

export default createHeaderDriver;
