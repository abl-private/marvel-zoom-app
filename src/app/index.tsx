import * as React from 'react';
import styled from 'styled-components';
import { MarvelApi } from '../api';
import { Project } from '../domain';
import { ImageScreen } from '../domain/index';
import Header from './header';
import ImageScreenView from './image-screen-view';

export type MarvelAppProps = {
  projectId: number;
  api: MarvelApi
};

type MarvelAppState = {
  project?: Project;
  selectedScreen?: ImageScreen;
  screenZoom: number;
  viewportWidth: number;
  viewportHeight: number;
};

export class MarvelApp extends React.Component<MarvelAppProps, MarvelAppState> {
  state: MarvelAppState = {
    screenZoom: 1,
    viewportWidth: 0,
    viewportHeight: 0,
  };

  componentDidMount() {
    this.props.api.getProjectById(this.props.projectId).then((project) => {
      const selectedScreen = project.screens[0];
      this.setState({ project, selectedScreen });
    });
  }

  getFittingZoom = (screen: ImageScreen) => Math.min(this.state.viewportWidth / screen.content.width, 1);

  getScreenById = (screenId: string | null) => {
    if (this.state.project && screenId) {
      return this.state.project.screens.find((screen) => screen.displayName === screenId) || null;
    } else return null;
  }

  onSelectScreen = (id: string) => {
    const selectedScreen = this.getScreenById(id);

    if (selectedScreen) {
      const optimalZoom = this.getFittingZoom(selectedScreen);
      this.setState({ selectedScreen, screenZoom: optimalZoom });
    } else {
      this.setState({ selectedScreen: undefined });
    }
  }

  onChangeZoom = (zoom: number) => this.setState({ screenZoom: zoom });

  setViewportDimensions = (vw: number, vh: number) => {
    this.setState({ viewportWidth: vw, viewportHeight: vh }, () => {
      const screen = this.state.selectedScreen;
      const screenZoom = screen ? this.getFittingZoom(screen) : this.state.screenZoom;
      this.setState({ screenZoom });
    });
  }

  render () {
    const { state } = this;

    return (
      <AppContainer>
        {state.selectedScreen ? (
          <>
            <Header
              project={state.project}
              selectedScreenId={state.selectedScreen.displayName}
              onSelectScreen={this.onSelectScreen}
              zoom={state.screenZoom}
              onChangeZoom={this.onChangeZoom}
            />
            <ImageScreenView
              value={state.selectedScreen}
              zoom={state.screenZoom}
              onSetViewSize={this.setViewportDimensions}
            />
          </>
        ) : null}
      </AppContainer>
    )
  }
}

const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`;
