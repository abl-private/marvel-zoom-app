import { BaseDriver } from '../../test-utils/base-driver';

export type ImageScreenViewDriver = {
  getImageWidth: () => number;
  getImageHeight: () => number;
}

const createImageScreenViewDriver = (base: BaseDriver): ImageScreenViewDriver => ({
  getImageHeight: () => parseInt(base.$('img').getAttribute('height')),
  getImageWidth: () => parseInt(base.$('img').getAttribute('width')),
});

export default createImageScreenViewDriver;
