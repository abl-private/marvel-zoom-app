import { ImageScreenViewProps } from './index';
import { ImageScreenViewDriver } from './driver';
import { buildImageScreen, buildImageScreenContent } from '../../domain/builders';
import { renderAndMountComponent } from '../../test-utils/index';
import ImageScreenView from './index';
import createImageScreenViewDriver from './driver';

describe('Image Screen View', () => {
  const setup = (partial: Partial<ImageScreenViewProps>): ImageScreenViewDriver => {
    const props: ImageScreenViewProps = {
      value: buildImageScreen(),
      zoom: 1,
      onSetViewSize: () => null,
      ...partial
    };

    const base = renderAndMountComponent(ImageScreenView, props);
    return createImageScreenViewDriver(base);
  };

  it('Calls onSetViewSize when mounted', () => {
    const onSetViewSize = jest.fn();
    setup({ onSetViewSize });
    expect(onSetViewSize).toHaveBeenCalled();
  });

  it('Renders image with calculated view and height dimensions based on zoom', () => {
    const content = buildImageScreenContent({ height: 450, width: 330 });
    const zoom = 0.34;
    const screen = buildImageScreen({ content });
    const driver = setup({ value: screen, zoom });

    expect(driver.getImageWidth()).toBeCloseTo(content.width * zoom, 0);
    expect(driver.getImageHeight()).toBeCloseTo(content.height * zoom, 0);
  });
})