import * as React from 'react';
import styled from 'styled-components';
import { ImageScreen } from '../../domain/index';

export type ImageScreenViewProps = {
  value: ImageScreen;
  zoom: number;
  onSetViewSize: (width: number, height: number) => void;
}

class ImageScreenView extends React.PureComponent<ImageScreenViewProps> {
  setViewSize = (elem: HTMLDivElement) => {
    const { width, height } = elem.getBoundingClientRect();
    this.props.onSetViewSize(width, height);
  };

  render () {
    const { url, height, width } = this.props.value.content;
    return (
      <ImageScreenViewContainer ref={this.setViewSize}>
        <ImageElem src={url} height={height * this.props.zoom} width={width * this.props.zoom}/>
      </ImageScreenViewContainer>
    )
  }
}

const ImageScreenViewContainer = styled.div`
  flex: 1;
  overflow: scroll;
  display: flex;
  justify-content: center;
`;

const ImageElem = styled.img`
  transform-origin: top;
`;

export default ImageScreenView;