import { BaseDriver } from '../test-utils/base-driver';
import { HeaderDriver } from './header/driver';
import createHeaderDriver from './header/driver';
import { ImageScreenViewDriver } from './image-screen-view/driver';
import createImageScreenViewDriver from './image-screen-view/driver';

export type MarvelAppDriver = {
  header: () => HeaderDriver;
  imageScreen: () => ImageScreenViewDriver
};

export const createMarvelAppDriver = (base: BaseDriver): MarvelAppDriver => ({
  header: () => createHeaderDriver(base),
  imageScreen: () => createImageScreenViewDriver(base),
});
