import * as React from 'react';
import { createMarvelGQLApi } from './api';
import { MarvelApp } from './app';

const appConfig = {
  marvelApiUrl: 'https://api.marvelapp.com/graphql/',
  marvelApiToken: '5glaD4ipXhUyYFeK1qys0M0xFJ4T2E',
  marvelProjectId: 4437271,
};

export class MarvelAppContainer extends React.Component {
  api = createMarvelGQLApi(appConfig.marvelApiUrl, appConfig.marvelApiToken);

  render() {
    return (
      <MarvelApp
        projectId={appConfig.marvelProjectId}
        api={this.api}
      />
    );
  }
}
