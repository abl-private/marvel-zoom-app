module.exports = function (wallaby) {
  return {
    files: [
      'src/**/*.ts?(x)',
      '!src/**/**/spec.tsx',
      '!src/**/**/spec.ts',
      'tsconfig.json'
    ],
    tests: [
      'src/**/**/spec.tsx',
      'src/**/**/spec.ts'
    ],
    env: {
      type: 'node'
    },
    testFramework: 'jest',
    debug: false,
  };
}