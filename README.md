# Marvel Zoom App

Completed using React, TypeScript & Styled Components.
Jest is used for testing.

## Running the app

1. Clone the repo
2. Install dependencies: `npm install`
3. Run tests: `npm run test`
4. Run the app: `npm run start`
5. Enjoy! :sparkles:
